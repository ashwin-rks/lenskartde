import pandas as pd

def process_csv(input_file, output_file):
    """
    Process the input CSV file by removing duplicates and NaN values,
    then save the cleaned data to a new CSV file.

    Parameters:
    input_file (str): The path to the input CSV file.
    output_file (str): The path to the output CSV file.
    """
    # Read the CSV file
    df = pd.read_csv(input_file)
    
    # Drop duplicates
    df = df.drop_duplicates()
    
    # Drop rows with NaN values
    df = df.dropna()
    
    # Save the cleaned DataFrame to a new CSV file
    df.to_csv(output_file, index=False)

# Process the CSV file
process_csv('products.csv', 'products_fullfinal.csv')