import scrapy

from lenskartde.items import StoreItem

class StoreSpider(scrapy.Spider):
    """Spider for scraping store data from Lenskart's website."""

    name = "store_spider"
    allowed_domains = ["www.lenskart.com"]

    custom_settings = {
        'isStorePipelineEnabled': True
    }

    def start_requests(self):
        """Generate initial request to the stores page."""
        yield scrapy.Request(url="https://www.lenskart.com/stores", callback=self.parse)

    def parse(self, response):
        """Parse the main stores page to extract location links."""
        locations = response.css('div#storeListingBase .AvailableStates_stateContainer__1_HiQ')

        for link in locations.css('a'):
            location_url = response.urljoin(link.attrib['href'])
            yield scrapy.Request(url=location_url, callback=self.parse_store)

    def parse_store(self, response):
        """Parse individual location pages to extract store details."""
        stores = response.css('div._state__subcontainer__y5RR2')
        for store_card in stores.css('div.StoreCard_storeCard__3lKXu'):
            store_info = store_card.css('div.StoreCard_imgContainer__P6NMN div.StoreCard_storeAddressContainer__pBYqN')
            
            store_item = StoreItem()
            store_item['type'] = 'store'
            store_item['name'] = store_info.css('.StoreCard_name__mrTXJ::text').get()
            store_item['address'] = store_info.css('a.StoreCard_storeAddress__PfC_v::text').get()
            
            rating_elements = store_info.css('.StoreCard_storeRatingContainer__onuBy span::text')
            store_item['rating'] = rating_elements[1].get() if rating_elements else '0'
            
            store_item['phone'] = store_info.css('.StoreCard_wrapper__xhJ0A a').attrib['href']
            yield store_item


