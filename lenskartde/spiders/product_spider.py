import scrapy
from lenskartde.items import ProductItem

BASE_API_URL = 'https://api-gateway.juno.lenskart.com/v2/products/category/'

class ProductSpider(scrapy.Spider):
    """Spider for scraping product data from Lenskart API."""

    name = "product_spider"
    allowed_domains = ["api-gateway.juno.lenskart.com", "www.lenskart.com"]

    def start_requests(self):
        """Generate initial requests for different product categories."""
        categories = [
            '3363', '8427', '8415', '3362', '16609', '16643', '16607', '16632', 
            '16630', '16553', '16631', '16633', '16634', '16641', '38760', '16637', 
            '16640', '16639', '16637', '16635', '16638', '16607', '16541', '4585', 
            '16522', '7769', '5571', '5572', '5573', '10413', '16632', '16638', 
            '16612', '4592', '"lens case"'
        ]
        for category in categories:
            url = f'{BASE_API_URL}{category}?page-size=15&page=1'
            yield scrapy.Request(url=url, callback=self.parse, meta={'category': category})

    def parse(self, response):
        """Parse the JSON response to extract product data."""
        data = response.json()
        category = response.meta['category']

        for product in data["result"]["product_list"]:
            product_item = ProductItem()
            product_item['type'] = 'product'
            product_item['id'] = product["id"]
            product_item['brand_name'] = product["brand_name"]
            product_item['model_name'] = product["model_name"]
            product_item['url'] = product["product_url"]
            product_item['price'] = product["prices"][1]["price"]
            product_item['rating'] = product["avgRating"]
            product_item['offer_name'] = product.get("offerName", "")
            yield product_item

        if data["result"]["num_of_products"] == 0:
            self.logger.info(f"Reached end for category {category}")
        else:
            next_page = int(response.url.split('=')[-1]) + 1
            next_url = f'{BASE_API_URL}{category}?page-size=15&page={next_page}'
            self.logger.info(f"Fetching next page for category {category}: {next_url}")
            yield scrapy.Request(url=next_url, callback=self.parse, meta={'category': category})
