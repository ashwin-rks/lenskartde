# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
# itemadapter import ItemAdapter
from scrapy.exceptions import DropItem, NotConfigured
    
class StorePipeline:
    def __init__(self) -> None:
        pass
    
    @classmethod
    def from_crawler(cls, crawler):
        if not crawler.settings.getbool('isStorePipelineEnabled'):
            raise NotConfigured
        return cls()
    
    def process_item(self, item, spider):
        item['phone'] = item['phone'].strip().replace('tel:', '')
        item['address'] = item['address'].strip()
        item['name'] = item['name'].strip()
        return item


