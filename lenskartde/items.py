# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class LenskartdeItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass

class ProductItem(scrapy.Item):
    type = scrapy.Field() 
    id = scrapy.Field() # unique id for each product
    brand_name = scrapy.Field() # Common brand name, smiliar for many products
    model_name = scrapy.Field() # Unique model name for each product
    url = scrapy.Field() # Product page url
    price = scrapy.Field() # Product's lenskart price
    rating = scrapy.Field() # Product's average rating
    offer_name = scrapy.Field() # Offer name

class StoreItem(scrapy.Item):
    type = scrapy.Field()
    name = scrapy.Field() 
    address = scrapy.Field()
    rating = scrapy.Field()
    phone = scrapy.Field()
