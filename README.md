# LensKartDE

## Getting started with lenskart scrapper

### How to run

1. Clone the repository
2. `cd lenskartde`
3. Install python 3.12.1 or higher
4. Create a venv using `python -m venv venv`
5. Activate venv in powershell using `.\venv\Scripts\Activate.ps1`
6. Install dependencies using `pip install -r requirements.txt`
7. `cd lenskartde`
8. To scrape stores `scrapy crawl store_spider -O stores.csv`
9. To scrape products `scrapy crawl product_spider -O products.csv`
